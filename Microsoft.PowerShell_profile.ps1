function prompt {
    if (!$?) {
        $e = [char]27
        $color = "1;31" # Red (Bold)
        Write-Host -NoNewline "$e[${color}mERR${e}[0m "
    }
    $location = Get-Location
    Write-Host -NoNewline ($location.toString().Replace($env:USERPROFILE, "~") + ">")
    return " "
}

function Get-ChildItemLongList {
    # list files,
    # sort directories and newest first,
    # show mode (attributes), length (size), lastwritetime,
    # colorize by attribute or extension

    $e = [char]27

    Write-Host -NoNewline "total"(Get-ChildItem -Force -Path $args).Count

    Get-ChildItem -Force -Path $args `
    | Sort-Object -Property Attributes, LastWriteTime `
    | Select-Object Mode, Length, `
    @{
        Name       = "LastWriteTimeISO"
        Expression = {
            $_.LastWriteTime.tostring("yyyy-MM-dd HH:mm")
        }
    }, `
    @{
        Name       = "Name"
        Expression = {
            $color = "0"
            $extension = [System.IO.Path]::GetExtension($_.Name)
            if ($_.Attributes -band [System.IO.FileAttributes]::Hidden) {
                $color = "0;100" # Black (High Intensity)
            }
            elseif ($_.Attributes -band [System.IO.FileAttributes]::Directory) {
                $color = "1;34" # Blue (Bold)
            }
            elseif ($extension -match ".(jpg|jpeg|png|jfif|webp|avif|webm|gif|bmp|svg)") {
                $color = "1;31" # Red (Bold)
            }
            elseif ($extension -match ".(txt)") {
                $color = "1;37" # White (Bold)
            }
            elseif ($extension -match ".(exe|ps1)") {
                $color = "1;33" # Yellow (bold)
            }
            elseif ($extension -match ".(zip|rar|7z)") {
                $color = "1;35" # Purple (bold)
            }
            "$e[${color}m$($_.Name)${e}[0m"
        }
    } `
    | Format-Table -AutoSize -HideTableHeaders -Wrap
}

Set-Alias -Name ll -Value Get-ChildItemLongList -Force
