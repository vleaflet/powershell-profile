# PowerShell Startup Script

## Features

### Prompt

Shortens the working directory that is displayed on prompt by replacing user profile directory (`C:\Users\user`) as `~`.

### Directory Listing

Custom columns, sorting and coloring.

Usage:

```
ll
```

![ll Screenshot](ll.png)

## Installation

Copy the contents of `Microsoft.PowerShell_profile.ps1` into the PowerShell profile indicated in `$PROFILE`.

Check whether the file already exists and it's location:

```
Get-ChildItem $PROFILE
```

## Links

[How to colorise PowerShell output of Format-Table](https://stackoverflow.com/questions/20705102/how-to-colorise-powershell-output-of-format-table)

[The entire table of ANSI color codes. ](https://gist.github.com/JBlond/2fea43a3049b38287e5e9cefc87b2124)

[Custom date and time format strings](https://learn.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings?redirectedfrom=MSDN)

[Expressions](https://learn.microsoft.com/en-us/powershell/scripting/lang-spec/chapter-07?view=powershell-7.3)

[FileAttributes Enum](https://learn.microsoft.com/en-us/dotnet/api/system.io.fileattributes?view=net-7.0)

[Multiline Command](https://shellgeek.com/powershell-multiline-command/)
